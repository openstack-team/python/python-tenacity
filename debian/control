Source: python-tenacity
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 9),
 dh-python,
 openstack-pkg-tools (>= 82~),
 python-all,
 python-pbr,
 python-setuptools,
 python-sphinx,
 python3-all,
 python3-pbr,
 python3-setuptools,
Build-Depends-Indep:
 python-concurrent.futures (>= 3.0),
 python-monotonic (>= 0.6),
 python-pytest,
 python-six,
 python-tornado,
 python3-pytest,
 python3-six,
 python3-tornado,
Standards-Version: 4.1.0
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-tenacity
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-tenacity.git
Homepage: https://github.com/jd/tenacity

Package: python-tenacity
Architecture: all
Depends:
 python-concurrent.futures (>= 3.0),
 python-monotonic (>= 0.6),
 python-pbr,
 python-six,
 python-tornado,
 ${misc:Depends},
 ${python:Depends},
Suggests:
 python-tenacity-doc,
Description: retry code until it succeeeds - Python 2.7
 Tenacity is a general-purpose retrying library to simplify the task of adding
 retry behavior to just about anything. It originates from a fork of Retrying.
 .
 Features:
  * Generic Decorator API
  * Specify stop condition (i.e. limit by number of attempts)
  * Specify wait condition (i.e. exponential backoff sleeping between attempts)
  * Customize retrying on Exceptions
  * Customize retrying on expected returned result
 .
 This package contains the Python 2.7 module.

Package: python-tenacity-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: retry code until it succeeeds - doc
 Tenacity is a general-purpose retrying library to simplify the task of adding
 retry behavior to just about anything. It originates from a fork of Retrying.
 .
 Features:
  * Generic Decorator API
  * Specify stop condition (i.e. limit by number of attempts)
  * Specify wait condition (i.e. exponential backoff sleeping between attempts)
  * Customize retrying on Exceptions
  * Customize retrying on expected returned result
 .
 This package contains the documentation.

Package: python3-tenacity
Architecture: all
Depends:
 python3-pbr,
 python3-six,
 python3-tornado,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-tenacity-doc,
Description: retry code until it succeeeds - Python 3.x
 Tenacity is a general-purpose retrying library to simplify the task of adding
 retry behavior to just about anything. It originates from a fork of Retrying.
 .
 Features:
  * Generic Decorator API
  * Specify stop condition (i.e. limit by number of attempts)
  * Specify wait condition (i.e. exponential backoff sleeping between attempts)
  * Customize retrying on Exceptions
  * Customize retrying on expected returned result
 .
 This package contains the Python 3.x module.
